﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace angle_container
{
    class Angle
    {
        private int _totalSeconds;
        public int Degrees
        {
            get
            {
                return _totalSeconds / 3600;
            }
        }
        public int Minutes
        {
            get
            {
                return _totalSeconds % 3600 / 60;
            }
        }
        public int Seconds
        {
            get
            {
                return _totalSeconds % 60;
            }
        }
        public Angle(int totalSeconds)
        {
            _totalSeconds = totalSeconds;
        }
        public Angle(int degrees, int minutes): this(degrees * 3600 + minutes * 60)
        {

        }
        public Angle(int degrees, int minutes, int seconds): this(degrees, minutes)
        {
            _totalSeconds += seconds;
        }
        public static Angle operator +(Angle lhr, Angle rhs)
        {
            return new Angle(lhr._totalSeconds + rhs._totalSeconds);
        }
        public static Angle operator -(Angle lhr, Angle rhs)
        {
            return new Angle(lhr._totalSeconds - rhs._totalSeconds);
        }
        public override string ToString()
        {
            bool isPositive = _totalSeconds > 0;
            return string.Format("{0}{1}*{2}'{3}''", isPositive ? "" : "-" , Math.Abs(Degrees), Math.Abs(Minutes), Math.Abs(Seconds));
        }
    }
}
